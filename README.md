AdobeWebM
=========

This is a project where I'm building plug-ins for Google's [WebM](http://www.webmproject.org/) format to be used inside the various Adobe applications.


Download
--------

[WebM plug-in on fnord.com](http://fnord.com)


License
-------
BSD


Author
------
Brendan Bolles